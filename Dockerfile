FROM node:lts-alpine AS builder
WORKDIR /app
COPY . .
RUN npm run clean
RUN npm install
RUN npm run build

FROM node:lts-alpine AS prod
WORKDIR /app
COPY --from=builder ./app/dist ./dist
COPY package* ./
RUN npm install --production
CMD npm start