
import { MongoClient, ObjectID } from 'mongodb';

export default class PageService {
    public connection: string;

    constructor(connection: string) {
        this.connection = connection;
    }

    async createPage(page: any) {
        const collection = await this.getCollection();
        const result = await collection.insertOne(page);

        return result;
    }

    async findPages() {
        const collection = await this.getCollection();
        const pages = await collection.find({}).toArray();

        return pages;
    }

    async findPage(id: string) {
        const collection = await this.getCollection();
        const page = await collection.findOne({_id: new ObjectID(id)});

        console.log('page', page);

        return page;
    }

    async updatePage(id: string, page: Object) {
        const collection = await this.getCollection();
        const result = await collection.updateOne({ _id: new ObjectID(id) }, { $set: page });

        return result;
    }

    async removePage(id: string) {
        const collection = await this.getCollection();
        const result = await collection.deleteOne({ _id: new ObjectID(id) });

        return result;
    }

    private async getCollection() {
        const client = await MongoClient.connect(this.connection);
        const db = client.db('improveasin');

        return db.collection('pages');
    }
}