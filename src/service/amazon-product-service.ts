import axios from 'axios';

export default class AmazonProductService {
    host: string;

    /**
     * @param host string
     */
    constructor(host: string) {
        this.host = host;
    }

    /**
     * @param asin string
     */
    async fetchProductByAsin(asin: string) {
        const uri = this.host + '/products/fetch-by-asin';
        const repsonse = await axios.post(uri, { asin });

        console.log('repsonse ', repsonse.data);

        return repsonse.data.product;
    }
}