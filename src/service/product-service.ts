
import { MongoClient } from 'mongodb';

export default class ProductService {
    async createProduct(page) {
        const collection = await this.getCollection();
        const result = await collection.insertOne(page);

        return result;
    }

    private async getCollection() {
        const client = await MongoClient.connect('mongodb+srv://root:5vGpWiAJnsovbOvA@cluster0-vwn1p.mongodb.net/improveasin?retryWrites=true&w=majority');
        const db = client.db('improveasin');

        return db.collection('products');
    }
}