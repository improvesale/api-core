export default class AmazonProduct {
    asin: string;

    constructor(asin: string) {
        this.asin = asin;
    }

    getAsin() {
        return this.asin;
    }
}