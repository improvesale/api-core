import * as express from 'express';
import * as bodyParser from "body-parser";
import * as cors from "cors";
import PageService from './service/page-service';
import AmazonProductService from "./service/amazon-product-service";

const amazonApiUrl = process.env.AMAZON_API_URL || 'http://api.improveasin.com/amazon';
const mongoConnection = process.env.MONGO_CONNECTION || 'mongodb+srv://root:5vGpWiAJnsovbOvA@cluster0-vwn1p.mongodb.net/improveasin?retryWrites=true&w=majority';

console.log('process.env.AMAZON_API_URL ', amazonApiUrl);
console.log('process.env.MONGO_CONNECTION ', mongoConnection);

const pageService = new PageService(mongoConnection);
const amazonProductService = new AmazonProductService(amazonApiUrl);

const app = express();

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.get('/ping', (req, res) => {
    res.send("pong")
});

app.get('/info', (req, res) => {
    res.json({
        version: 'v1',
        project: 'improveASIN',
        service: 'api-core',
        language: 'node',
        type: 'api'
    })
})

app.get('/pages', async (req, res) => {
    const data = await pageService.findPages();

    res.json({ data });
});
app.get('/pages/:pageId', async (req, res) => {
    const pageId = req.params.pageId;
    const item = await pageService.findPage(pageId);

    console.log('pageId', pageId);
    console.log('item', item);

    res.json({ item });
});
app.post('/pages', async (req, res) => {
    console.log('req', req.body);

    const asin = req.body.asin;
    if (!asin) {
        return res.status(404);
    }
    let amazonProduct = await amazonProductService.fetchProductByAsin(asin);
    const result = await pageService.createPage(amazonProduct);

    return res.json({ _id: result.insertedId });
});
app.delete('/pages/:id', async (req, res) => {
    const id = req.param("id");
    if (!id) {
        return res.status(404);
    }

    const result = await pageService.removePage(id);
    return res.json({ result });
});

app.listen(3000, () => {
    console.log('listen to port 3000');
});